from rest_framework import serializers
from .models import Order, OrderLine


class OrderLineSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderLine
        fields = ('id', 'product', 'qty', )


class OrderSerializer(serializers.ModelSerializer):

    lines = OrderLineSerializer(many=True)
    user = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Order
        fields = ('id', 'user', 'phone', 'lines', )

    def create(self, validated_data):
        lines = validated_data.pop('lines')
        instance = Order.objects.create(**validated_data)
        for order_line in lines:
            instance.lines.create(**order_line)

        return instance
