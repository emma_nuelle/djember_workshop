from django.db import models
from django.conf import settings

from catalog.models import Product


class Order(models.Model):

    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    phone = models.CharField(max_length=20)
    created = models.DateTimeField(auto_now_add=True)

    @property
    def total(self):
        return sum([line.product.price * line.qty for line in self.lines.all()])

    @property
    def title(self):
        return 'Order #{}'.format(self.id)

    def __str__(self):
        return '{} (${})'.format(self.title, self.total)


class OrderLine(models.Model):

    order = models.ForeignKey(Order, related_name='lines')
    product = models.ForeignKey(Product)
    qty = models.PositiveIntegerField()

    def __str__(self):
        return '{} x {}'.format(self.qty, self.product.name)
