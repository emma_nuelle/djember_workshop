from django.contrib import admin

from .models import Order, OrderLine


class OrderLineInline(admin.TabularInline):

    model = OrderLine
    extra = 0
    fields = ('qty', 'product', )


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):

    list_display = ['title', 'total', ]
    inlines = [OrderLineInline, ]

    def get_queryset(self, request):
        return super(OrderAdmin, self).get_queryset(request) \
            .prefetch_related('lines', 'lines__product')
