import random

from factory import fuzzy, SubFactory, Faker, lazy_attribute
from factory.django import DjangoModelFactory
from ..models import Category, Product


class CategoryFactory(DjangoModelFactory):

    class Meta:
        model = Category

    name = fuzzy.FuzzyText(prefix='Category ', length=3)


LOREM_CATEGORIES = [
    'food',
    'animals',
    'technics',
]


class ProductFactory(DjangoModelFactory):

    class Meta:
        model = Product

    name = Faker('word')
    category = SubFactory(CategoryFactory)
    description = Faker('text')
    image = lazy_attribute(lambda o: 'http://lorempixel.com/200/300/{}'.format(
        random.choice(LOREM_CATEGORIES)
    ))
    price = fuzzy.FuzzyDecimal(5)
