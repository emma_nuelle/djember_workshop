from .factories import ProductFactory, CategoryFactory
from my_webshop.tests.base import BaseAPITestCase
from rest_framework.test import APITestCase
from ..models import Product, Category
from ..serializers import ProductSerializer, CategorySerializer


class CategoryAPITest(BaseAPITestCase, APITestCase):

  api_base_name = 'category'
  model = Category
  model_factory_class = CategoryFactory
  serializer_class = CategorySerializer

  api_is_read_only = True


class ProductAPITest(BaseAPITestCase, APITestCase):

  api_base_name = 'product'
  model = Product
  model_factory_class = ProductFactory
  serializer_class = ProductSerializer

  api_is_read_only = True
