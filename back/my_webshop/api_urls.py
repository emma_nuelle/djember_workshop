from django.conf.urls import include, url
from rest_framework import routers

from .views import UserViewSet
from catalog.views import CategoryViewSet, ProductViewSet
from checkout.views import OrderViewSet


router = routers.DefaultRouter()

router.register(r'userinfos', UserViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'products', ProductViewSet)
router.register(r'orders', OrderViewSet)

urlpatterns = [
    url(r'', include(router.urls)),
]
