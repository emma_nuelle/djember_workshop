from django.contrib.auth import get_user_model, authenticate

from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = (
            'id',
            'username',
            'email',
            'first_name',
            'last_name'
        )


class LoginSerializer(serializers.Serializer):

    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        try:
            user = authenticate(username=data['username'], password=data['password'])
            if user and user.is_active:
                data['user'] = user
                return data
        except get_user_model().DoesNotExist:
            pass
        raise serializers.ValidationError('Wrong username or password')


class UserCreateSerializer(serializers.ModelSerializer):

    password = serializers.CharField(min_length=8, write_only=True)
    password_confirmation = serializers.CharField(write_only=True)

    class Meta:
        model = get_user_model()
        fields = (
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'password',
            'password_confirmation'
        )

    def validate(self, data):
        if data.get('password') != data.get('password_confirmation'):
            raise serializers.ValidationError("Confirmation doesn't match password")
        data.pop('password_confirmation')
        return data
