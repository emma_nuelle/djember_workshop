import Ember from 'ember';

export default Ember.Service.extend({
  store: Ember.inject.service(),
  items: Ember.computed(function() {
    return this.get('store').peekAll('basketitem');
  }).volatile(),

  last_update: undefined,
  set_updated() {
    this.set('last_update', new Date());
  },

  append(product, qty) {
    this.get('store').query('basketitem', {filter: { product: product.id }})
      .then((items) => {
        let item = items.get('firstObject');
        if (item) {
          item.set('qty', item.get('qty') + qty);
        } else {
          item = this.get('store').createRecord('basketitem', {
            product,
            qty
          });
        }
        item.save();
        this.set_updated();
      });
  },

  count: Ember.computed('last_update', function() {
    return this.get('items').reduce(function(sum, item) {
      return sum + item.get('qty');
    }, 0);
  }),

  total: Ember.computed('last_update', function() {
    return this.get('items').reduce(function(sum, item) {
      return sum + item.get('qty') * item.get('product.price');
    }, 0);
  }),

  clear() {
    this.get('items').map((item) => {
      item.destroyRecord();
    });
    this.set_updated();
  },

  remove(item) {
    item.destroyRecord();
    this.set_updated();
  },

  init() {
    this.get('store').findAll('basketitem').then((items) => {
      items.map((item) => {
        item.destroyRecord();
      });
    });
    this.set_updated();
  }
});
