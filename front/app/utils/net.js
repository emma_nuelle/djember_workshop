import Ember from 'ember';
import Cookies from 'ember-cli-js-cookie';
import fetch from 'ember-network/fetch';

function isSecureUrl(url) {
  var link  = document.createElement('a');
  link.href = url;
  link.href = link.href;
  return link.protocol === 'https:';
}

export function makeRequest(url, data) {
  if (!isSecureUrl(url)) {
    Ember.Logger.warn('Credentials are transmitted via an insecure connection' +
       '- use HTTPS to keep them secure.');    
  }
  
  return new Ember.RSVP.Promise((resolve, reject) => {
    fetch(url, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        "X-CSRFToken": Cookies.get('csrftoken'),
        "Content-Type": 'application/json',
      }
    }).then((response) => {
      if (response.status === 400) {
        response.json().then((json) => {
          reject(json);
        });
      } else if (response.status > 400) {
        reject(response);
      } else {
        resolve(response);
      }
    }).catch((err) => {
      reject(err);
    });
  });
}
