import Ember from 'ember';
import Base from 'ember-simple-auth/authenticators/base';
import config from '../config/environment';
import fetch from 'ember-network/fetch';
import { makeRequest } from '../utils/net';


export default Base.extend({

  init() {
    var globalConfig = config['ember-simple-auth'] || {};
    this.serverAuthEndpoint = globalConfig.serverAuthEndpoint || '/rest-auth';
  },

  authenticate(credentials) {
    return new Ember.RSVP.Promise((resolve, reject) => {
      const data = { username: credentials.identification, password: credentials.password };
      makeRequest(`${this.serverAuthEndpoint}/login/`, data).then((response) => {
        Ember.run(() => {
          resolve(response);
        });
      }, (xhr /*, status, error */) => {
        Ember.run(() => {
          reject(xhr.responseJSON || xhr.responseText);
        });
      });
    });
  },

  restore(data) {
    return new Ember.RSVP.Promise((resolve, reject) => {
      fetch(`${this.serverAuthEndpoint}/me/`).then((/* response */) => {
        resolve(data);
      }, (/* xhr , status, error */) => {
        reject();
        this.invalidate();
      });
    });
  },

  invalidate(/* data */) {
    function success(resolve) {
      resolve();
    }
    return new Ember.RSVP.Promise((resolve /*, reject */) => {
      makeRequest(`${this.serverAuthEndpoint}/logout/`, {}).then((/* response */) => {
        Ember.run(() => {
          success(resolve);
        });
      }, (/* xhr, status, error */) => {
        Ember.run(() => {
          success(resolve);
        });
      });
    });
  },
});
