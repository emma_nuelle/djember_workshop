import Ember from 'ember';

export default Ember.Route.extend({
  templateName: 'index/index',
  model(params) {
    return this.get('store').query('product', {category_id: params.category_id});
  }
});
