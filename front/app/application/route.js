import Ember from 'ember';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';
import sections from '../sections';

export default Ember.Route.extend(ApplicationRouteMixin, {
  authenticator: 'authenticator:django',
  cart: Ember.inject.service(),
  
  model() {
    return sections;
  },

  sessionAuthenticated() {
    // Prevent default transition by doing nothing
  },

  actions: {
    addToCart(product) {
      this.get('cart').append(product, 1);
    },

    removeFromCart(item) {
      this.get('cart').remove(item);
    },

    authenticate(credentials) {
      const session = this.get('session');
      if (credentials.identification && credentials.password) {
        session.authenticate(this.get('authenticator'), credentials).then(() => {
          session.set('loginError', false);
        }, () => {
          session.set('loginError', "Invalid credentials. Please retry.");
        });
      }
    },

    invalidate() {
      this.get('session').invalidate()
      .catch((err) => {
        const session = this.get('session');
        session.set('registrationError', err);      
      });
    }
  }
});
