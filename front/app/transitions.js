export default function() {
  this.transition(
    this.fromRoute('product'),
    this.use('explode', {    
      matchBy: 'data-product-id',     
      use: ['fly-to', {duration: 300}]
    }, {
      matchBy: 'data-image-id',
      use: ['fly-to', {duration: 300}]
    }, {
      use: ['fade', {duration: 150}]  
    })
  );
  this.transition(
    this.toRoute('product'),
    this.use('explode', {
      matchBy: 'data-product-id',
      use: ['fly-to', { duration: 300 }]
    }, {
      matchBy: 'data-image-id',
      use: ['fly-to', {duration: 300 }]
    }, {
      use: ['fade', { duration: 150 }]
    })
  );
  this.transition(
    this.fromRoute('basket'),
    this.toRoute('order'),
    this.use('toLeft'),
    this.reverse('toRight')
  );
  this.transition(
    this.toRoute('basket'),
    this.use('toDown')
  );
  this.transition(
    this.fromRoute('order'),
    this.use('toUp')
  );
  this.transition(
    this.use('fade', { duration: 150  }),
    this.debug()
  );
}   
