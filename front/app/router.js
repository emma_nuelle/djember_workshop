import Ember from 'ember';
import config from './config/environment';
import sections from './sections';

const Router = Ember.Router.extend({
  location: config.locationType
});

export default Router.map(function() {
  sections.forEach((section) => {
    const opts = section.opts || {};
    if (section.route !== 'index') {
      this.route(section.route, opts);
    }
  });
  this.route('login');
  this.route('logout');
  this.route('index', {
    path: '/'
  }, function() {
    this.route('category', {
      path: '/category/:category_id'
    });
  });
  this.route('product', {
    path: '/product/:product_id'
  });
  this.route('basket');
  this.route('order');
});
