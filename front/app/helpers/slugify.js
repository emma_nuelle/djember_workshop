import Ember from 'ember';
import { slugify } from '../utils/text';

export function slugifyHelper(params/*, hash*/) {
  return slugify(params[0]);
}

export default Ember.Helper.helper(slugifyHelper);
