import Ember from 'ember';
import { titlize } from '../utils/text';

export function titlizeHelper(params/*, hash*/) {
  return titlize(params[0]);
}

export default Ember.Helper.helper(titlizeHelper);
