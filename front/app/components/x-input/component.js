import Ember from 'ember';

export default Ember.Component.extend({
  field_name: '',
  type: 'text',

  errors: {},

  has_error: Ember.computed('field_name', 'errors', function() {
    const field_name = this.get('field_name');
    const errors = this.get('errors');
    return errors && errors.hasOwnProperty(field_name);
  })
});
