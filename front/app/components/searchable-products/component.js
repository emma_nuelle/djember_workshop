import Ember from 'ember';

export default Ember.Component.extend({
  searchTerm: '',
  filteredProducts: Ember.computed('searchTerm', 'model.length', function() {
  	const searchTerm = this.get('searchTerm').toLowerCase();
  	const products = this.get('model');
  	return products.filter((item) => {
      return item.get('isLoaded') &&
  	    item.get('name').toLowerCase().indexOf(searchTerm) > -1;
  	});
	})
});
