import Ember from 'ember';

export default Ember.Component.extend({
  session: Ember.inject.service(),

  actions: {
    login(username, password) {
      this.authenticate({identification: username, password});
    }
  },

  onInit: function() {
    this.set('session.loginError', false);
    this.set('session.registrationError', false);
  }.on('init')
});
