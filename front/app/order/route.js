import Ember from 'ember';

export default Ember.Route.extend({
  cart: Ember.inject.service(),
  flashMessages: Ember.inject.service(),
  model() { return this.get('cart'); },

  actions: { 
    saveOrder(phone) {
      const record = this.get('store').createRecord('order', { phone });
      const lines = record.get('lines');
      this.get('cart.items').forEach((line) => {
        lines.pushObject(
          this.get('store').createRecord('orderline', {
            product: line.get('product'), qty: line.get('qty') })
        );
      });
      record.save().then(() => {
        this.transitionTo('index');
        Ember.run.later(() => {
          this.get('cart').clear();
        }, 300);
        this.get('flashMessages').success('Thank you for your order');
      }).catch(() => {
        this.get('flashMessages').danger('something went wrong!');
      });
    }
  }
});
