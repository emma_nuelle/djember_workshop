import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { hasMany, belongsTo } from 'ember-data/relationships';

export default Model.extend({
  user: belongsTo('userinfo', {
    async: true
  }),
  phone: attr('string'),
  lines: hasMany('orderline')  
});
