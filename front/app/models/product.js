import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo } from 'ember-data/relationships';

export default Model.extend({
  name: attr('string'),
  category: belongsTo('category', {
    async: true
  }),
  description: attr('string'),
  image: attr('string'),
  price: attr('number')
});
