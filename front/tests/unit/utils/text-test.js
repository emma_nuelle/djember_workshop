import { slugify, titlize } from 'my-webshop/utils/text';
import { module, test } from 'qunit';

module('Unit | Utility | text');
  
test('slugify works', function(assert) {
  let result = slugify('Az- erty-UIOP*$ù');
  assert.equal(result, 'az-erty-uiop');
});

test('titlize works', function(assert) {
  let result = titlize('first_name');
  assert.equal(result, 'First Name');
});
