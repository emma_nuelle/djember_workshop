import { makeRequest } from 'my-webshop/utils/net';
import { module, test  } from 'qunit';
import startMirage from '../../helpers/setup-mirage-for-integration';

module('Unit | Utility | net', {
  beforeEach() {
    startMirage(this.container);
  }
});

// Replace this with your real tests.
test('it works', function(assert) {
  let result = makeRequest('/bogus_route', {});
  assert.ok(result);
});
