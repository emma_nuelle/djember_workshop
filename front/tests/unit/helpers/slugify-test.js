import { slugifyHelper } from 'my-webshop/helpers/slugify';
import { module, test } from 'qunit';

module('Unit | Helper | slugify');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = slugifyHelper(['42']);
  assert.ok(result);
});
