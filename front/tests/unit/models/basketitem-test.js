import { moduleForModel, test } from 'ember-qunit';

moduleForModel('basketitem', 'Unit | Model | basketitem', {
  // Specify the other units that are required for this test.
  needs: ['serializer:basketitem', 'model:product']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
