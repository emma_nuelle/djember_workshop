import Ember from 'ember';

import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';


const cartStub = Ember.Service.extend({
  count: 0
});

moduleForComponent('shopping-cart', 'Integration | Component | shopping cart', {
  integration: true,

  beforeEach() {
    this.register('service:cart', cartStub);
    this.inject.service('cart');
  }
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{shopping-cart}}`);

  assert.equal(this.$().text().trim(), '');

  Ember.set(cartStub, 'count', 10);
  Ember.set(cartStub, 'items', [undefined]);
  this.set('cartStub', cartStub);
  this.render(hbs`{{shopping-cart cart=cartStub}}`);

  assert.equal(this.$().text().trim(), '10');
});
