import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import { manualSetup, make } from 'ember-data-factory-guy';
import { contains  } from '../../../helpers/sl/synchronous';

moduleForComponent('searchable-products', 'Integration | Component | searchable products', {
  integration: true,

  beforeEach: function() {                          
    manualSetup(this.container);                    
  }
});

test('it renders', function(assert) {
  let model = [];

  for(let i=0; i<2; i++) {
    const c = make('category');
    const ct = Math.floor((Math.random * 2) + 2);
    for(let j=0; j<ct; j++) {
      model.push(make('product', { category: c  }));
    }
  }

  this.set('model', model);
  this.render(hbs`{{searchable-products model=model}}`);

  assert.equal(this.$().text().trim(), '');

  assert.equal(this.$().text().trim(), '');
      
  const found = make('product',{name: 'Find me'});
  const hidden = make('product', {name: 'Hide me'});
    
  model = [found, hidden];
    
  this.set('model', model);    
  this.render(hbs`{{searchable-products model=model searchTerm='fiNd'}}`);
  
  assert.ok(contains(this.$().text(), 'Find me'));
  assert.notOk(contains(this.$().text(), 'Hide me'));
});
