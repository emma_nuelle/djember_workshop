import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

function doNothing() {}

moduleForComponent('login-or-register', 'Integration | Component | login or register', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.set('doNothing', doNothing);
  this.render(hbs`
              {{#login-or-register authenticate=doNothing}}
                template block text
              {{/login-or-register}}
             `);

  assert.equal(this.$().text().trim().replace(/\s+/g, '').replace('\n', ''),
               'ORLoginUsernamePasswordLoginORRegisterUsernameEmailFirstName' +
               'LastNamePasswordPasswordConfirmationRegister' +
               'templateblocktext');
});
