import FactoryGuy from 'ember-data-factory-guy';
import { faker } from 'ember-cli-mirage';

FactoryGuy.define('product', {
  sequences: {                                     
    productName: (num) => {                        
      return `Product ${num}`;                     
    }
  },
  default: {                                       
    name: FactoryGuy.generate('productName'),      
    category: FactoryGuy.belongsTo('category'),    
    description: faker.lorem.paragraph,            
    image: faker.image.imageUrl,                   
    price: faker.commerce.price                    
  }
});
