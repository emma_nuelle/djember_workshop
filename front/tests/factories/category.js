import FactoryGuy from 'ember-data-factory-guy';

FactoryGuy.define('category', {
  sequences: {
    categoryName: (num) => {                       
      return `Category ${num}`;                    
    }
  },
  default: {                                       
    name: FactoryGuy.generate('categoryName')      
  }
});
